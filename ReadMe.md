
# Project parent-config - Parent Project

## About project parent-config

```text
统一依赖关系、统一子项目配置风格。
```

### Pom Project

```text
POM 项目是为了方便被继承，通过 config 项目来统一管理依赖jar关系。
```

### Build - resources

```text
配置指定资源目录的打包文件类型和排除的文件类型。
```

### Build - plugins

```text
通过插件配置来管理编译。
```

### Profiles

```text
配置了五套环境配置，默认情况下是dev(develop)开发环境配置。
Maven 编译打包时通过 -P {Profile-id} 来指定环境配置文件。

<filtering>true</filtering> 开启变量替换，在 properties 配置文件中自动替换 Maven 变量。

```

### Dependency Management

```text
统一依赖管理，子项目自动继承，使用时仅需要指定 groupId 和 artfactId 。
```

